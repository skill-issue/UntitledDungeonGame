﻿using System;
using System.IO;
using osu.Framework.Allocation;
using osu.Framework.Graphics;
using osuTK;
using UnnamedDungeonGame.Game.Screens;
using UnnamedDungeonGame.Game.IO;
using UnnamedDungeonGame.Game.Entities;
using UnnamedDungeonGame.Game.Input;
using Jint.Native;
using System.Collections.Generic;

namespace UnnamedDungeonGame.Game
{
    /// <summary>
    /// The game, as well as its handling for its menus and execution,
    /// building on top off <see cref="DungeonGameBase"/> for its initial components.
    /// </summary>
    public class DungeonGame : DungeonGameBase
    {
        public static DungeonGame SELF;
        public JavaScriptState JSState;
        public DungeonScreenStack ScreenStack;

        private readonly string[] args;

        public bool EmittedReady = false;

        public DungeonGame()
        {
            SELF = this;
        }

        public DungeonGame(string[] args = null)
        {
            this.args = args;
            SELF = this;
        }

        [BackgroundDependencyLoader]
        private void Load()
        {
            AddInternal(new PlayerInputContainer()
            {
                Anchor = Anchor.Centre,
                Origin = Anchor.Centre,
                RelativeSizeAxes = Axes.Both,
                Size = new Vector2(1366, 768),
                Child = new Player()
            });
        }

        protected override void LoadComplete()
        {
            base.LoadComplete();

            JSState = new JavaScriptState(this);

            // TODO: recursively handle the game's logic
            var gameDirectory = AppDomain.CurrentDomain.BaseDirectory;
            var gameDataFolder = Path.Join(gameDirectory, "game");
            var gameJSFile = Path.Join(gameDataFolder, "game.js");
            var gameJSData = File.ReadAllText(gameJSFile);

            JSState.Execute(gameJSData);
            
            JSState.EventEmitter.Emit("start", JsValue.FromObject(JSState.State, new Dictionary<string, object>()
            {
                { "delta", Time.Elapsed }
            }));
        }

        protected override void UpdateAfterChildren()
        {

            base.UpdateAfterChildren();
        }
    }
}
