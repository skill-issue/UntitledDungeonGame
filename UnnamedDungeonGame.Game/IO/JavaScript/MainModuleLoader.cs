﻿using System;
using System.Collections.Generic;
using System.IO;
using Esprima.Ast;
using Jint;
using Jint.Runtime.Modules;

namespace UnnamedDungeonGame.Game.IO.JavaScript;

public class MainModuleLoader : IModuleLoader
{
    public Dictionary<string, object> ModuleReferences => new Dictionary<string, object>();

    public string BaseDirectory => AppDomain.CurrentDomain.BaseDirectory;

    public MainModuleLoader()
    {

    }

    public Module LoadModule(Engine engine, ResolvedSpecifier resolved)
    {
        Console.WriteLine(resolved.Key);
        throw new System.NotImplementedException();
    }

    public ResolvedSpecifier Resolve(string? referencingModuleLocation, string specifier)
    {
        return new ResolvedSpecifier(referencingModuleLocation ?? "", specifier ?? "", null, SpecifierType.Bare);
    }
}
