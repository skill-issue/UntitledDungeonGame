﻿using Jint.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnnamedDungeonGame.Game.IO.JavaScript
{
    public class JavaScriptEmitter
    {
        public Dictionary<string, List<JsValue>> Emitters;
        
        public JavaScriptEmitter()
        {
            Emitters = new Dictionary<string, List<JsValue>>();
        }

        public void On(string eventName, JsValue callback)
        {
            if (!Emitters.ContainsKey(eventName))
            {
                Emitters.Add(eventName, new List<JsValue>() { callback });
            }
            else
            {
                Emitters[eventName].Add(callback);
            }
        }
        
        public void Emit(string eventName, params JsValue[] args)
        {
            if (!Emitters.ContainsKey(eventName))
            {
                return;
            }

            foreach (JsValue callback in Emitters[eventName])
            {
                Console.WriteLine(callback);
                JavaScriptState.SELF.State.Invoke(callback, args);
            }
        }   
    }
}
