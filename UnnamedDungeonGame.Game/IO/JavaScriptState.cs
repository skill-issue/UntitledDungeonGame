﻿using System;
using System.Collections.Generic;
using Jint;
using Jint.Native;
using Jint.Runtime;
using osuTK;
using osu.Framework.Graphics.Shapes;
using UnnamedDungeonGame.Game.IO.JavaScript;
using osu.Framework.Graphics;
using Jint.Runtime.Debugger;
using System.Reflection;
using osu.Framework.Logging;
using Jint.Native.Error;
using UnnamedDungeonGame.Game.Entities;

namespace UnnamedDungeonGame.Game.IO {
    public class JavaScriptState
    {
        public static JavaScriptState SELF;

        public readonly Engine State;

        private DungeonGame game;

        private DebugInformation _debugInfo;

        private int _currentExecutionChunkNumber = 0;

        private Dictionary<int, string> _currentExecutionChunkNames = new Dictionary<int, string>();

        public JavaScriptEmitter EventEmitter;

        public JavaScriptState(DungeonGame _game)
        {
            JavaScriptState.SELF = this;
            game = _game;

            // Create our JavaScript state
            State = new Engine(o =>
            {
                // engine options
                o.DebugMode(true);
                o.AllowClr(Assembly.GetEntryAssembly());
                o.CatchClrExceptions(o => true);
                o.EnableModules(new MainModuleLoader());
                o.Modules.RegisterRequire = true;
            });
            
            State.DebugHandler.Step += debugHandlerStep;

            // Create a new event emitter
            EventEmitter = new JavaScriptEmitter();

            registerGlobalModules();
        }

        private StepMode debugHandlerStep(object sender, DebugInformation e)
        {
            _debugInfo = e;

            return StepMode.Over;
        }

        public void Execute(string code)
        {
            string name;
            if (_currentExecutionChunkNumber == 0)
            {
                name = "game";
            } else
            {
                name = $"chunk_${_currentExecutionChunkNumber}";
            }

            Execute(name, code);
        }

        public void Execute(string name, string code)
        {
            int executionNumber = _currentExecutionChunkNumber;

            try
            {
                // increment
                _currentExecutionChunkNames[_currentExecutionChunkNumber] = name;
                _currentExecutionChunkNumber += 1;

                // run
                State.Execute(code);
            } catch (Exception e)
            {
                if (e is JavaScriptException)
                {
                    e = e as JavaScriptException;
                    // TODO: superior error logger, i hate this and its existence
                    Logger.Log((e as JavaScriptException).ToString()
                        .Replace("<anonymous", _currentExecutionChunkNames[executionNumber])
                        .Replace("Jint.Runtime.JavaScriptException", ((e as JavaScriptException).Error as ErrorInstance).Prototype.Get("name").AsString()));
                } else
                {
                    throw;
                }
            }
        }


        public void RegisterObject<T>()
        {

        }

        public void RegisterModule(string specifier, Action<ModuleBuilder> builder)
        {
            State.AddModule(specifier, builder);
        }

        private void registerGlobalModules()
        {
            // osu!framework
            RegisterModule("osu-framework", builder =>
            {
                // osu!framework object types
                builder.ExportType<Box>();

                // osuTK types
                builder.ExportType<Vector2>();
            });

            // Graphics Handling
            RegisterModule("game", builder =>
            {
                builder.ExportType<Player>();

                builder.ExportObject("events", EventEmitter);
                
                // Drawing functions
                builder.ExportObject("graphics", new Dictionary<string, object>()
                {
                    ["draw"] = draw
                });

                // Logging Functions
                builder.ExportObject("logger", new Dictionary<string, object>()
                {
                    ["raw"] = rawLog
                });
            });

            RegisterModule("events", builder =>
            {
                builder.ExportType<JavaScriptEmitter>("EventEmitter");
            });
        }

        // Private Methods

        private void rawLog(string value)
        {
            Console.WriteLine(value);
        }

        private JsValue draw(object obj)
        {
            if (!(obj is Drawable))
            {
                throw new JavaScriptException(State.Realm.Intrinsics.TypeError, "The argument provided to the draw function is not a valid Drawable object.");
            }

            game.Add(obj as Drawable);
            return JsValue.FromObject(State, obj as Drawable);
        }
    }
}
