﻿using System;
using System.Collections.Generic;
using osu.Framework.Allocation;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Containers;
using osu.Framework.Graphics.Shapes;
using osu.Framework.Input.Bindings;
using osu.Framework.Input.Events;
using osuTK;
using osuTK.Graphics;
using Jint.Native;
using UnnamedDungeonGame.Game.Input;
using UnnamedDungeonGame.Game.IO.JavaScript;

namespace UnnamedDungeonGame.Game.Entities
{
    public class Player : Entity, IKeyBindingHandler<PlayerAction>
    {
        public PlayerStatus Status = PlayerStatus.InGame;

        public JavaScriptEmitter Emitter = new JavaScriptEmitter();

        private KeyDownEvent currentKeyDown;
        private KeyUpEvent currentKeyUp;

        public Player()
        {
            
        }


        [BackgroundDependencyLoader]
        private void Load()
        {
            Add(new Box()
            {
                Anchor = Anchor.Centre,
                Origin = Anchor.Centre,
                Colour = Color4.Red,
                Size = new Vector2(150, 150),
            });
        }

        protected override bool OnKeyDown(KeyDownEvent e)
        {
            currentKeyDown = e;
            return base.OnKeyDown(e);
        }

        protected override void OnKeyUp(KeyUpEvent e)
        {
            currentKeyUp = e;
            base.OnKeyUp(e);
        }

        public bool OnPressed(KeyBindingPressEvent<PlayerAction> e)
        {
            if (Status == PlayerStatus.InGame)
            {
                Emitter.Emit("keyDown", JsValue.FromObject(DungeonGame.SELF.JSState.State, new Dictionary<string, object>()
                {
                    ["key"] = currentKeyDown.Key,
                    ["action"] = e.Action
                }));
            }
            
            return true;
        }

        public void OnReleased(KeyBindingReleaseEvent<PlayerAction> e)
        {
            Console.WriteLine("owo");
            if (Status == PlayerStatus.InGame)
            {
                Console.WriteLine("owo2");
                Emitter.Emit("keyUp", JsValue.FromObject(DungeonGame.SELF.JSState.State, new Dictionary<string, object>()
                {
                    ["key"] = currentKeyUp.Key,
                    ["action"] = e.Action
                }));
            }
        }
    }

    public enum PlayerStatus {
        Loading,
        InGame
    }
}
