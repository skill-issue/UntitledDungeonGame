﻿using System;
using System.Collections.Generic;
using osu.Framework.Input.Bindings;

namespace UnnamedDungeonGame.Game.Input
{
    public class PlayerInputContainer : KeyBindingContainer<PlayerAction>
    {
        public PlayerInputContainer(KeyCombinationMatchingMode keyCombinationMatchingMode = KeyCombinationMatchingMode.Any, SimultaneousBindingMode simultaneousBindingMode = SimultaneousBindingMode.All)
            : base(simultaneousBindingMode, keyCombinationMatchingMode)
        {
        }

        // TODO: aly is a nerd
        public override IEnumerable<KeyBinding> DefaultKeyBindings => new[]
        {
            new KeyBinding(InputKey.W, PlayerAction.MoveUp),
            new KeyBinding(InputKey.A, PlayerAction.MoveLeft),
            new KeyBinding(InputKey.S, PlayerAction.MoveRight),
            new KeyBinding(InputKey.D, PlayerAction.MoveDown),
        };
    }

    public enum PlayerAction {
        MoveLeft,
        MoveRight,
        MoveUp,
        MoveDown
    }
}
