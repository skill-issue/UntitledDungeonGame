﻿using osu.Framework.Allocation;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Containers;
using osu.Framework.Platform;
using UnnamedDungeonGame.Game.Input;

namespace UnnamedDungeonGame.Game
{
    /// <summary>
    /// Class to handle any relevant resource store, and underlying settings.
    /// </summary>
    public partial class DungeonGameBase : osu.Framework.Game
    {
        private Container content;
        
        [BackgroundDependencyLoader]
        private void Load(GameHost host)
        {
            base.Content.Add(new GlobalActionContainer()
            {
                RelativeSizeAxes = Axes.Both,
                Child = content = new DrawSizePreservingFillContainer()
            });
        }
    }
}
