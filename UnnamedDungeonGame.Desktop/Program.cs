﻿using osu.Framework;
using osu.Framework.Platform;
using System;
using UnnamedDungeonGame.Game;

namespace UnnamedDungeonGame.Desktop
{
    internal class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            using (GameHost host = Host.GetSuitableDesktopHost(@"unnameddungeongame")) {
                using (osu.Framework.Game game = new DungeonGame(args))
                {
                    host.Run(game);
                }
            }
        }
    }
}
